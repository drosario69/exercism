package com.erazurdo.exercism.simplelinkedlist;

import java.lang.reflect.Array;
import java.util.NoSuchElementException;

public class SimpleLinkedList<T> {
	private Integer nodes;
	private Node head = null;

	public SimpleLinkedList() {
		nodes = 0;
	}

	public SimpleLinkedList(T[] arr) {
		nodes = 0;
		for(T t: arr) {
			push(t);
		}
	}

	public Integer size() { return nodes; }

	private class Node {
		public T value;
		public Node next = null;
	}

	public T pop() {
		if(nodes == 0) {
			throw new NoSuchElementException("List is empty");
		}
		T val = head.value;
		head = nodes-- > 1 ? head.next : null;
		return val;
	}

	public void push(T val) {
		Node ptr = new Node();
		ptr.value = val;
		ptr.next = nodes++ == 0 ? null : head;
		head = ptr;
	}

	public void reverse() {
		if(nodes <= 1) {
			return;
		}
		// this is somewhat like "push", but we will be inserting
		Node ptr = head.next, next, prev = head;
		while(ptr != null) {
			next = ptr.next; // could be null, but that's fine

			// do the reversal
			ptr.next = prev;
			prev = ptr;
			ptr = next;
		}
		head = prev;
	}

	public T[] asArray(Class<T> t) {
		T[] arr = (T[]) Array.newInstance(t, nodes);
		int idx = 0;
		Node ptr = head;
		while(ptr != null) {
			arr[idx++] = ptr.value;
			ptr = ptr.next;
		}
		return arr;
	}
}
