# Simple Linked List

## Instructions
Write a simple linked list implementation that uses Elements and a List.

The linked list is a fundamental data structure in computer science, often used in the implementation of other data structures. They're pervasive in functional programming languages, such as Clojure, Erlang, or Haskell, but far less common in imperative languages such as Ruby or Python.

The simplest kind of linked list is a singly linked list. Each element in the list contains data and a "next" field pointing to the next element in the list of elements.

This variant of linked lists is often used to represent sequences or push-down stacks (also called a LIFO stack; Last In, First Out).

As a first take, lets create a singly linked list to contain the range (1..10), and provide functions to reverse a linked list and convert to and from arrays.

When implementing this in a language with built-in linked lists, implement your own abstract data type.

## Solution

Linked lists are essentially a data structure holding a value and a pointer to the next data structure:

```
structure:
- data
- pointer to next structure
```

Traversal is straight forward:

A -> B -> C -> D

To reach C, one must start with A and travel until C is reached.

Whether we add to the end of the list or insert at the beginning of the list is arbitrary; either method achieves the same result. However, when adding elements to the tail, traversal to the tail is required, whereas inserting at the beginning of the list requires no traversal. If we maintain a pointer to the tail, however, we overcome the traversal requirement.

It is beneficial to wrap the data structure in order to be able to maintain metadata as well as hiding the actual implementation of the list:

```
wrapper:
- metadata 1
- metadata 2
- structure:
  - data
  - pointer to next structure
```

![tests_passed](images/tests_passed.png)

![intellij](images/intellij.png)
